var platformer = platformer || {};

platformer.jumper_prefab = function(game,x,y,pointA,pointB,speed,direction,level){
    Phaser.Sprite.call(this,game,x,y,'jumper');
    this.anchor.setTo(.5);
    this.animations.add('walk',[0,1,2,3],10,true);
    this.animations.play('walk');
    this.patrolA = pointA;
    this.patrolB = pointB;
    this.speed = speed;
    this.direction = direction;
    this.level = level;
    this.game.physics.arcade.enable(this);

};

platformer.jumper_prefab.prototype = Object.create(Phaser.Sprite.prototype);
platformer.jumper_prefab.prototype.constructor = platformer.jumper_prefab;


platformer.jumper_prefab.prototype.update= function(){
    //WALLS
    this.game.physics.arcade.collide(this,this.level.walls);
    if(this.body.blocked.right || this.body.blocked.left){
        this.direction *=-1;
        this.scale.x = this.direction;
    }
    this.body.velocity.x = this.speed*this.direction;
    //HERO
    this.game.physics.arcade.collide(this,this.level.hero,this.hitHero, null, this);
};

platformer.jumper_prefab.prototype.hitHero= function(enemy,hero){
    if(enemy.body.touching.up && hero.body.touching.down){
        this.kill();
        hero.body.velocity.y = -gameOptions.heroJump;
        //particulas de sangre
    }else{
        hero.reset(65,100);
        this.level.camera.shake(0.05,250);
        this.level.camera.flash(0xFF0000,500);
    }
};






