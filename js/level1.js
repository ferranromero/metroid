var platformer = platformer || {};

platformer.level1={
    init:function(){
        this.scale.scaleMode= Phaser.ScaleManager.SHOW_ALL;
        this.scale.setGameSize(gameOptions.gameWidth/2,gameOptions.gameHeight/2);
        
       this.game.physics.startSystem(Phaser.Physics.ARCADE);
       this.game.physics.arcade.gravity.y=gameOptions.heroGravity;
        
        this.game.world.setBounds(0,0,gameOptions.level1Width,gameOptions.level1Height);
    },
    preload:function(){
        var ruta='assets/img/';
        this.load.image('bg',ruta+'bg_green_tile.png');
       this.load.tilemap('level1','assets/tilemaps/level1.json',null,Phaser.Tilemap.TILED_JSON);
       this.load.image('walls',ruta+'tileset_wall_lv1.png');
       this.load.image('edges',ruta+'tileset_edge_lv1.png');
        
        this.load.spritesheet('hero',ruta+'hero.png',32,32);
        this.load.spritesheet('door',ruta+'door.png',32,40);
        this.load.spritesheet('jumper',ruta+'jumper.png',32,32);
        this.load.spritesheet('slime',ruta+'slime.png',32,32);
        this.load.spritesheet('hud_energy',ruta+'hud_energy.png',128,28);
        
    },
    create:function(){
        this.bg=this.game.add.tileSprite(0,0,gameOptions.level1Width,gameOptions.level1Height,'bg');
        this.map=this.game.add.tilemap('level1');
        this.map.addTilesetImage('walls');
        this.walls=this.map.createLayer('walls_layer');
        this.map.setCollisionBetween(1,11,true,'walls_layer');
        
        this.map.addTilesetImage('edges');
        this.map.createLayer('left_edges');
        this.map.createLayer('right_edges');
        this.map.createLayer('up_edges');
        this.map.createLayer('bottom_edges');
        
      
        
        this.entry=this.game.add.sprite(65,268,'door',0);
        this.entry.anchor.setTo(.5);
        this.game.physics.arcade.enable(this.entry);
        this.entry.body.allowGravity=false;
        this.entry.body.immovable = true;
        this.entry.animations.add('open',[1,2,3],4,true);
        this.entry.animations.play('open');
          
        this.hero=this.game.add.sprite(665,100,'hero',0);
        this.hero.anchor.setTo(.5);
        this.hero.animations.add('run',[2,3,4,5],10,true);
        this.game.physics.arcade.enable(this.hero);
        this.hero.health = 6;
        
        this.hud_energy = this.game.add.sprite(10,10,'hud_energy',this.hero.health);
        this.hud_energy.fixedToCamera = true;
        
        this.cursors= this.game.input.keyboard.createCursorKeys();
        this.space=this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        
        //this.camera.follow(this.hero);
        //this.camera.follow(this.hero,Phaser.Camera.FOLLOW_PLATFORMER);
        this.camera.follow(this.hero,Phaser.Camera.FOLLOW_LOCKON);
        //this.jumper = new platformer.jumper_prefab(this.game,340,100,240,368,100,1,this);
        this.jumper = new platformer.jumper(this.game,100,100);
        
        this.game.add.existing(this.jumper);
        
        //this.slime = new platformer.slime_prefab(this.game,772,270,740,970,50,1,this);
        //this.game.add.existing(this.slime);
        
    },
    update:function(){
        //this.jumper.run();
       // this.game.physics.arcade.collide(this.hero,this.entry);
        this.game.physics.arcade.collide(this.hero,this.walls);
        
        if(this.cursors.left.isDown){
            this.hero.body.velocity.x=-gameOptions.heroSpeed;
            this.hero.animations.play('run');
            //this.hero.scale.setTo(-1,1);
            this.hero.scale.x=-1;
        }else
        if(this.cursors.right.isDown){
           this.hero.animations.play('run');
            this.hero.scale.setTo(1); this.hero.body.velocity.x=gameOptions.heroSpeed;
        }else{
            this.hero.frame=0;
            this.hero.body.velocity.x=0;
        }
        //this.hero.body.onFloor()
        if(this.space.isDown 
           && this.hero.body.blocked.down
           && this.space.downDuration(250)){
            this.hero.body.velocity.y=-gameOptions.heroJump;
        }
        if(!this.hero.body.blocked.down){
            this.hero.frame=1;
        }
        
        
        
        
        
        
    }
};